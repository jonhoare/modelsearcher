﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ModelSearcher")]
[assembly: AssemblyDescription("Model Searcher is a simple tool I created for my own use to be able to perform a text based search across specific properties on my Models/ViewModels. I quickly realised that if I made it generic, this tool could be a great benefit to others. Then, after building a List of ISearchableModels you can call .Search(searchText) which will use Linq.Dynamic to build a search criteria based on all of the [Searchable] properties for that ISearchableModel and will perform the search, returning you the filtered List. You can also Target a particular Searchable Property on your model to search, by calling .Search(m => m.PropertyToSearch, searchText). This will then cause a search on the particular PropertyToSearch as long as it is attributed as Searchable. Currently this only searches across Properties that are either string/int/long. Other types to come soon! Source code and Wiki available at https://bitbucket.org/jonhoare/modelsearcher!")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Livewire-UK")]
[assembly: AssemblyProduct("LivewireUK.ModelSearcher")]
[assembly: AssemblyCopyright("Copyright © Livewire-UK 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("541c7e0d-49a5-466a-b488-d76b22d8fc04")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.0.1")]
[assembly: AssemblyFileVersion("2.0.0.1")]
[assembly: AssemblyInformationalVersion("2.0.0.1")]
