﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using LivewireUK.ModelSearcher.Attributes;
using LivewireUK.ModelSearcher.Interfaces;

namespace LivewireUK.ModelSearcher
{
    public static class SearchHelper
    {
        public static List<TModel> Search<TModel>(this List<TModel> TModelList, string searchText) where TModel : ISearchableModel
        {
            return Search(TModelList, null, searchText);
        }
        public static List<TModel> Search<TModel>(this List<TModel> TModelList, Expression<Func<TModel, object>> targetField, string searchText) where TModel : ISearchableModel
        {
            searchText = searchText.ToLower();
            var modelQuery = TModelList.AsQueryable();
            var modelList = new List<TModel>();
            var searchQueryBuilder = new StringBuilder();
            MemberExpression target = null;

            if (targetField != null)
                target = targetField.Body as MemberExpression ?? ((UnaryExpression) targetField.Body).Operand as MemberExpression;

            var properties = typeof(TModel).GetProperties().Where(prop => prop.GetCustomAttributes(typeof(SearchableAttribute), false).Length > 0 && (targetField == null || (target != null && prop.Name == target.Member.Name)));

            int searchId = 0;
            if (int.TryParse(searchText, out searchId))
                properties.Where(prop => prop.PropertyType == typeof(int)).ToList().ForEach(prop => searchQueryBuilder.Append("(").Append(prop.Name).Append(" != null AND ").Append(prop.Name).Append(".Equals(@0)) OR "));

            long searchLong = 0;
            if (long.TryParse(searchText, out searchLong))
                properties.Where(prop => prop.PropertyType == typeof(long)).ToList().ForEach(prop => searchQueryBuilder.Append("(").Append(prop.Name).Append(" != null AND ").Append(prop.Name).Append(".Equals(@0)) OR "));
            
            var searchQuery = searchQueryBuilder.ToString();

            if (searchQuery.Contains("OR"))
                searchQuery = searchQuery.Remove(searchQuery.LastIndexOf("OR") - 1);

            if (searchId > 0)
                modelList.AddRange(modelQuery.AsQueryable().Where(searchQuery, searchId).ToList());
            else if (searchLong > 0)
                modelList.AddRange(modelQuery.AsQueryable().Where(searchQuery, searchLong).ToList());
            
            properties.Where(prop => prop.PropertyType == typeof(string)).ToList().ForEach(prop => searchQueryBuilder.Append("(").Append(prop.Name).Append(" != null AND ").Append(prop.Name).Append(".ToLower().Contains(@0)) OR "));

            searchQuery = searchQueryBuilder.ToString();

            if (searchQuery.Contains("OR"))
                searchQuery = searchQuery.Remove(searchQuery.LastIndexOf("OR") - 1);

            var searchResult = modelQuery.AsQueryable().Where(searchQuery, searchText).ToList();
            modelList.AddRange(searchResult);

            return !string.IsNullOrEmpty(searchText) ? modelList.Distinct().ToList() : modelQuery.ToList();
        }
    }
}
