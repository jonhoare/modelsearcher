﻿using System;
using System.Collections.Generic;
using LivewireUK.ModelSearcher;
using LivewireUk.ModelSearcher.Test.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LivewireUk.ModelSearcher.Test
{
    [TestClass]
    public class ModelSearcherTest
    {
        private readonly List<TestModel> _testModelList = new List<TestModel>();
        public ModelSearcherTest()
        {
            _testModelList.Add(new TestModel
                                   {
                                       Age = 26,
                                       FirstName = "Jon",
                                       LastName = "Hoare",
                                       Town = "London",
                                       NonSearchableString = "Billy",
                                       SpecialDate = new DateTime(2000,01,01)
                                   });
            _testModelList.Add(new TestModel
                                   {
                                       Age = 22,
                                       FirstName = "Jim",
                                       LastName = "Bob",
                                       Town = "York 26",
                                       NonSearchableString = "Billy",
                                       SpecialDate = new DateTime(2013,01,01)
                                   });
            _testModelList.Add(new TestModel
                                   {
                                       Age = 26,
                                       FirstName = "Sue",
                                       LastName = "Simmers",
                                       Town = "Southampton",
                                       NonSearchableString = "Billy",
                                       SpecialDate = new DateTime(1980,01,01)
                                   });
            _testModelList.Add(new TestModel
                                   {
                                       Age = 26,
                                       FirstName = "Billy",
                                       LastName = "No'Mates",
                                       Town = "London",
                                       SpecialDate = new DateTime(2000,01,01)
                                   });
            _testModelList.Add(new TestModel
                                    {
                                        Age = 26,
                                        FirstName = "Jean",
                                        LastName = "Billy",
                                        Town = "London",
                                        SpecialDate = new DateTime(2000, 01, 01)
                                    });
        }

        [TestMethod]
        public void ShouldFind2LondonResults()
        {
            var resultsList = _testModelList.Search("London");

            Assert.AreEqual(2, resultsList.Count);
        }

        [TestMethod]
        public void ShouldFind1JonResults()
        {
            var resultsList = _testModelList.Search("Jon");

            Assert.AreEqual(1, resultsList.Count);
        }

        [TestMethod]
        public void ShouldTargetFirstNameSearchOnly()
        {
            var resultsList = _testModelList.Search(m => m.FirstName, "Billy");

            Assert.AreEqual(1, resultsList.Count);
        }

        [TestMethod]
        public void ShouldReturn4Age26()
        {
            var resultsList = _testModelList.Search(m => m.Age, "26");

            Assert.AreEqual(4, resultsList.Count);
        }

        [TestMethod]
        public void ShouldReturn5x26()
        {
            var resultsList = _testModelList.Search("26");

            Assert.AreEqual(5, resultsList.Count);
        }
    }
}
