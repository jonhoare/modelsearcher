﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LivewireUK.ModelSearcher.Attributes;
using LivewireUK.ModelSearcher.Interfaces;

namespace LivewireUk.ModelSearcher.Test.Models
{
    public class TestModel : ISearchableModel
    {
        [Searchable]
        public string FirstName { get; set; }
        [Searchable]
        public string LastName { get; set; }
        [Searchable]
        public string Town { get; set; }

        public string NonSearchableString { get; set; }

        [Searchable]
        public int Age { get; set; }
        [Searchable]
        public DateTime SpecialDate { get; set; }
    }
}
